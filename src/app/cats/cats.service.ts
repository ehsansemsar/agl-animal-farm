import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import 'rxjs/add/operator/map';

@Injectable()
export class CatsService {

  private _url: string = 'http://agl-developer-test.azurewebsites.net/people.json';

  constructor(private _http: HttpClient) {
  }

  readCats(): Observable<Pet[]> {
    return this._http.get<IOwner[]>(this._url)
      .map(response => {
        return this.getSortedCatList(response);
      });
  }

  private getSortedCatList(response) {
    const ownerList = response;
    const catList = new Array<Pet>();

    this.extractCats(ownerList, catList);

    return this.sortCatsByName(catList);
  }

  private extractCats(ownerList: any[], catList: Pet[]) {
    _.forEach(ownerList, (owner: IOwner) => {
      _.forEach(owner.pets, function (pet: Pet) {
        if (pet.type === 'Cat') {
          catList.push(new Pet(owner.gender, pet.name));
        }
      });
    });
  }

  private sortCatsByName(catList: any) {
    return _.sortBy(catList, ['name']);
  }
}

export interface IOwner {
  name: string;
  gender: string;
  /*TODO: Use enum for gender*/
  pets: Pet[];
}

export class Pet {
  type: string;
  /*TODO: Use enum for type*/
  ownerGender: string;
  /*TODO: Use enum for gender*/
  name: string;

  constructor(ownerGender: string, name: string) {
    this.ownerGender = ownerGender;
    this.name = name;
  }
}
