import {Component, OnInit} from '@angular/core';
import {CatsService} from './cats.service';
import * as _ from 'lodash';

@Component({
  selector: 'agl-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.scss']
})
export class CatsComponent implements OnInit {
  public catList = [];
  public maleCatList = [];
  public femaleCatList = [];

  constructor(private _catService: CatsService) {
  }

  ngOnInit() {
  }

  readCats() {
     this._catService.readCats()
      .subscribe(
        response => {
          this.catList = response;
          this.maleCatList = _.filter(this.catList, {ownerGender: 'Male'});
          this.femaleCatList = _.filter(this.catList, {ownerGender: 'Female'});
        },
        err => console.error(err)
      )
  }
}


