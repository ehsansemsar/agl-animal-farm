import { async, TestBed } from '@angular/core/testing';
import { CatsComponent } from './cats.component';
import {CatsService} from './cats.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('CatsComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CatsService]
    })
    .compileComponents();
  }));

  it('should create cats component', () => {
    expect(true).toBe(true);
  });
});
