import {TestBed, inject} from '@angular/core/testing';
import {CatsService} from './cats.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('CatsService', () => {

  let catsService: CatsService;
  let httpMock: HttpTestingController;
  let url: string = 'http://agl-developer-test.azurewebsites.net/people.json';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CatsService]
    });

    catsService = TestBed.get(CatsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be 7 cats in the farm', () => {

    catsService.readCats()
      .subscribe(res => {
        expect(res.length).toEqual(7);
      });

    let req = httpMock.expectOne(url);
    expect(req.request.method).toBe('GET');
    req.flush(dummyResponse);


  });

  it('should catsService returns only cat names', () => {

    catsService.readCats()
      .subscribe(res => {
        res.forEach(pet => {
          expect(catNames.indexOf(pet.name) >= 0).toBeTruthy();
        });
      });

    let req = httpMock.expectOne(url);
    expect(req.request.method).toBe('GET');
    req.flush(dummyResponse);

  });

  it('should catsService returns cats in alphabetical order', () => {
    catsService.readCats()
      .subscribe(res => {
        var prev: string = res[0].name;

        res.forEach(pet => {
          expect(pet.name >= prev).toBeTruthy();
          prev = pet.name;
          console.log(prev);
        });
      });

    let req = httpMock.expectOne(url);
    expect(req.request.method).toBe('GET');
    req.flush(dummyResponse);

  });

  const dummyResponse = [{
    'name': 'Bob',
    'gender': 'Male',
    'age': 23,
    'pets': [{'name': 'Garfield', 'type': 'Cat'}, {'name': 'Fido', 'type': 'Dog'}]
  }, {'name': 'Jennifer', 'gender': 'Female', 'age': 18, 'pets': [{'name': 'Garfield', 'type': 'Cat'}]}, {
    'name': 'Steve',
    'gender': 'Male',
    'age': 45,
    'pets': null
  }, {
    'name': 'Fred',
    'gender': 'Male',
    'age': 40,
    'pets': [{'name': 'Tom', 'type': 'Cat'}, {'name': 'Max', 'type': 'Cat'}, {'name': 'Sam', 'type': 'Dog'}, {
      'name': 'Jim',
      'type': 'Cat'
    }]
  }, {'name': 'Samantha', 'gender': 'Female', 'age': 40, 'pets': [{'name': 'Tabby', 'type': 'Cat'}]}, {
    'name': 'Alice',
    'gender': 'Female',
    'age': 64,
    'pets': [{'name': 'Simba', 'type': 'Cat'}, {'name': 'Nemo', 'type': 'Fish'}]
  }];

  const catNames = ['Garfield', 'Tom', 'Max', 'Jim', 'Tabby', 'Simba'];


});
