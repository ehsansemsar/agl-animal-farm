import { Component } from '@angular/core';

@Component({
  selector: 'agl-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AGL Animal Farm';
}
